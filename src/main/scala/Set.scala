/**
 * Created by mohannaddafrawy on 4/25/14.
 */
class Set(player1:String,player2:String)  {

  def player1Name: String = {
    val x: String = player1
     x
  }


  def player2Name: String = {
    val x: String = player2
     x
  }


  val points = new Points
  val print = new PrintScore


  def score: String = {

    if (hasWinner) {
      playerWithHighestScore + " wins."
    } else if (hasAdvantage) {
       "Advantage " + playerWithHighestScore
    } else if (isDeuce) {
       "Deuce"
    } else {
      "%s score: %s; %s score: %s".format(player1Name,translateScore(points.player1Points),player2Name,translateScore(points.player2Points))
    }

  }


  def isDeuce: Boolean = {

    points.player1Points >= 3 && points.player2Points == points.player1Points

  }


  def playerWithHighestScore: String = {
    if (points.player1Points > points.player2Points ) {
       player1Name
    }
    else{

      player2Name
    }
  }


  def hasWinner: Boolean = {
    if(points.player2Points >= 4 && points.player2Points >= points.player1Points + 2 )
      true
   else
    if(points.player1Points >= 4 && points.player1Points >= points.player2Points + 2)
      true
    else
      false
  }


  def hasAdvantage : Boolean = {
    if (points.player2Points >= 4 && points.player2Points >= points.player1Points + 1)
      true
   else
    if (points.player1Points >= 4 && points.player1Points >= points.player2Points + 1)
      true
   else
     false

  }


  def player1Score() {

     points.player1Score1Point
     print.printScore(score)

}


  def player2Score() {

    points.player2Score1Point
    print.printScore(score)

  }


  def translateScore(score: Int): String = score match {
      case 0 => "0"
      case 1 => "15"
      case 2 => "30"
      case 3 => "40"
      case 4 => "Love"
  }
}


trait PrintScoreInterface {

def printScore(score:String):Unit

}


class PrintScore extends PrintScoreInterface{

   def printScore(score:String):Unit =
    println(score)


}