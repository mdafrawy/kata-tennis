/**
 * Created by mohannaddafrawy on 4/25/14.
 */
object Test extends App {

   // Create a set
  val set = new Set("Mohannad","Hussein")

   // Get result
  println("The initial result: " + set.score)

   // Get player names
  println("Player 1 name is: " + set.player1Name)
  println("Player 2 name is: " + set.player2Name)

   // Player A scores
  set.player1Score()

   // Player B scores
  set.player2Score()

  // Get result
  println("The result: " + set.score)

  //The game
  set.player2Score()
  set.player1Score()
  set.player2Score()
  set.player1Score()
  set.player2Score()
  set.player1Score()
  set.player2Score()
  set.player2Score()


}
